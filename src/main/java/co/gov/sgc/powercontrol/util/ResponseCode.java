package co.gov.sgc.powercontrol.util;

/**
 *
 * @author Julian Peña
 */
public enum ResponseCode {
    OK, INVALID, TIMEOUT;
}
