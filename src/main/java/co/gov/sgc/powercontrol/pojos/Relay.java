package co.gov.sgc.powercontrol.pojos;

/**
 *
 * @author Julian Peña
 */
public class Relay {
    
    private double id = -1;
    private String name = "default";
    private String code = "S01";
    private boolean status = true;
    private double stationId = 0;

    public Relay(double id, String name, String code, boolean status, double stationID) {
        this.id = id;
        this.code = code;
        this.name = name;
        this.status = status;
        this.stationId = stationID;
    }

    public Relay() {
    }
    

    /**
     * 
     * @return the relay database id
     */
    public double getId() {
        return id;
    }

    /**
     * Sets the database id (careful this should be read only!)
     * @param id 
     */
    public void setId(double id) {
        this.id = id;
    }

    /**
     * 
     * @return The relay name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the relay name
     * @param name 
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 
     * @return The relay code
     */
    public String getCode() {
        return code;
    }

    /**
     * Sets the relay code
     * @param code 
     */
    public void setCode(String code) {
        this.code = code;
    }
    
    /**
     * 
     * @return the relay last recorded status
     */
    public boolean getStatus() {
        return status;
    }

    /**
     * Sets the relay status
     * @param status 
     */
    public void setStatus(boolean status) {
        this.status = status;
    }
    
    /**
     * 
     * @return the relay database station id 
     */
    public double getStationId() {
        return stationId;
    }

    /**
     * Sets the database station id for the relay
     * @param stationId 
     */
    public void setStationId(double stationId) {
        this.stationId = stationId;
    }
    
    
    
}
