package co.gov.sgc.powercontrol.verticles;

import co.gov.sgc.powercontrol.util.ResponseCode;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.net.NetClient;
import io.vertx.core.net.NetSocket;

/**
 *
 * @author Julian Peña
 */
public class TCPClientVerticle extends AbstractVerticle {

    public static final String ADDRESS = "TCPClientAddress";

    //Timeout in milliseconds
    private static final int TIMEOUT = 15 * 1000;

    //Response lenght in bytes, no more bytes are read from a socket
    private static final int RESPONSE_MAX_LENGH = 2;

    @Override
    public void start() throws Exception {

        super.start();

        vertx.eventBus().consumer(ADDRESS, addressHandler -> {
            String[] message = ((String) addressHandler.body()).split(",");
            String ip = message[0];
            int port = Integer.parseInt(message[1]);
            String command = message[2];
            sendMessage(ip, port, command, addressHandler.replyAddress());
        });

    }

    /**
     * Connects to the host via TCP, sends the given message and waits for a
     * response. If no response is received before 15 seconds then the sockets
     * is closed-
     *
     * @param ip
     * @param port
     * @param message
     * @param replyAddress The vertx event bus address on which the response
     * should be sent
     */
    private void sendMessage(String ip, int port, String message, String replyAddress) {
        
        //TODO Check if not connection is currently active to the same ip and port
        //Delay connection by X seconds if needed
        
        NetClient netClient = vertx.createNetClient();
        netClient.connect(port, ip, connectHandler -> {
            if (connectHandler.failed()) {
                vertx.eventBus().send(LoggerVerticle.ADDRESS, "Couldn't connecto to TCP Server");
            } else {

                NetSocket socket = connectHandler.result();

                //After connection wait 1 second before sending the command
                vertx.setTimer(1000, sendHandler -> {

                    //Timeout handling
                    long timerID = vertx.setTimer(TIMEOUT, timeOuthandler -> {
                        //socket.close();
                        netClient.close();
                        vertx.eventBus().send(replyAddress, ResponseCode.TIMEOUT.toString());
                    });

                    StringBuilder response = new StringBuilder();

                    //Receive handler
                    socket.handler(buffer -> {
                        response.append(buffer.toString().trim());
                        if (response.length() >= RESPONSE_MAX_LENGH) {
                            vertx.cancelTimer(timerID);
                            vertx.eventBus().send(replyAddress, response.toString());
                            //socket.close();
                            netClient.close();
                        }
                    });

                    //Send the command
                    socket.write(message);
                });
            }
        });
    }

}
