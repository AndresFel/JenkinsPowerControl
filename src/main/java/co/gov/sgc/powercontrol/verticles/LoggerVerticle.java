package co.gov.sgc.powercontrol.verticles;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.json.Json;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Julian Peña
 */
public class LoggerVerticle extends AbstractVerticle {

    public static final String ADDRESS = "LoggerVerticleAddress";
    public static final String LOG = "Log";

    private RandomAccessFile appLogFile = null;

    @Override
    public void start() throws Exception {

        super.start();

        appLogFile = new RandomAccessFile(new File("power-control.log"), "rw");

        vertx.eventBus().consumer(ADDRESS, logAddresshandler -> {
            String message = ((String) logAddresshandler.body()).trim();
            if (message.length() > 0) {
                log(message);
            }
        });
        //consumer mostrar log 
        vertx.eventBus().consumer(LOG, handler -> {
            try {
                leerContenidoLog(handler.replyAddress());
            } catch (IOException ex) {
                Logger.getLogger(LoggerVerticle.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
    }

    /**
     * Appends the given message to the global log file including a timestamp.
     *
     * @param message
     */
    private void log(String message) {
        try {
            Timestamp timestamp = new Timestamp(System.currentTimeMillis());
            appLogFile.seek(appLogFile.length());
            appLogFile.writeBytes(timestamp.toString() + "\t\t" + message + "\n");
        } catch (IOException ex) {
            Logger.getLogger(LoggerVerticle.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    /**
     * lee el archivo log linea por linea y las guarda en un arrayList
     * @param replyAddress
     * @throws FileNotFoundException
     * @throws IOException 
     */
    private void leerContenidoLog(String replyAddress) throws FileNotFoundException, IOException {
        ArrayList<String> log = new ArrayList<>();
        Runtime r = Runtime.getRuntime();
        Process p = r.exec("tail -100 power-control.log");
        Scanner s = new Scanner(p.getInputStream());
        while (s.hasNextLine()) {
            log.add(s.nextLine());
        }
        s.close();
        vertx.eventBus().send(replyAddress, Json.encode(log));
    }

}
