package co.gov.sgc.powercontrol.verticles;

import co.gov.sgc.powercontrol.pojos.Relay;
import co.gov.sgc.powercontrol.pojos.Station;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.StaticHandler;
import io.vertx.ext.web.templ.JadeTemplateEngine;

/**
 *
 * @author ANDRESFELIPE
 */
public class HttpServer extends AbstractVerticle {

    private EventBus eventbus;

    @Override
    public void start() {

        eventbus = vertx.eventBus();

        Router r = Router.router(vertx);
        JadeTemplateEngine jade = JadeTemplateEngine.create();
        r.route("/webroot/*").handler(StaticHandler.create("webroot"));

        r.get("/").handler(context -> {
            jade.render(context, "webroot/index.jade", respuesta -> {
                if (respuesta.succeeded()) {
                    context.response().end(respuesta.result());
                } else {
                    context.fail(respuesta.cause());
                }
            });
        });

        //enrrutamiento para crud estaciones
        r.get("/api/Estaciones").handler(this::getStation);
        r.route("/api/Estaciones*").handler(BodyHandler.create());
        r.post("/api/Estaciones").handler(this::addStation);
        r.get("/api/Estaciones/:idStation").handler(this::getRelays);
        r.put("/api/Estaciones/:id").handler(this::updateStation);
        r.delete("/api/Estaciones/:id").handler(this::delStation);
        //enrutamiento para todo el crud de relays
        r.route("/api/Relays*").handler(BodyHandler.create());
        r.post("/api/Relays").handler(this::addRelay);
        r.put("/api/Relays/:id").handler(this::editRelay);
        r.delete("/api/Relays/:id").handler(this::delRelay);
        r.post("/api/Relays/:idRelay").handler(this::onOffRelay);
        r.get("/api/Relays/:stationId").handler(this::getCodes);
        //enrutamiento para el LOG
        r.get("/api/Log").handler(this::getLog);
        // Serve the non private static pages
        r.route().handler(StaticHandler.create());
        // start a HTTP web server on port 8090
        vertx.createHttpServer().requestHandler(r::accept).listen(8090);
    }
    /**
     * obetien las lineas del archivo log 
     * @param r 
     */
    private void getLog(RoutingContext r){
        eventbus.send(LoggerVerticle.LOG, "", Handler -> {
            if (Handler.failed()) {
                
            } else {
                String [] log = Json.decodeValue(Handler.result().body().toString(), String[].class);
                r.response()
                        .putHeader("content-type", "application/json; charset=utf-8")
                        .end(Json.encodePrettily(log));
            }
        });
    }
    /**
     * obtiene los codes en uso en la bd por estacion
     * @param r 
     */
    private void getCodes(RoutingContext r){
        String stationId = r.request().getParam("stationId");
        eventbus.send(DBClientVerticle.QUERY_GET_CODE, stationId, Handler ->{
            if (Handler.failed()) {
                
            }else{
                String [] code = Json.decodeValue(Handler.result().body().toString(),String[].class);
                r.response()
                        .putHeader("content-type", "application/json; charset=utf-8")
                        .end(Json.encodePrettily(code));
            }
        });
    }

    /**
     * senal para encender o apagar un relay
     * @param r
     */
    private void onOffRelay(RoutingContext r) {
        String idRelay = r.request().getParam("idRelay");
        eventbus.send(DBClientVerticle.QUERY_GET_RELAY, idRelay, Handler -> {
            if (Handler.failed()) {
//                eventbus.send(LoggerVerticle.ADDRESS, "Fail get relay");
            } else {
                Relay relay = Json.decodeValue((String) Handler.result().body(), Relay.class);
                eventbus.send(DBClientVerticle.QUERY_GET_STATION, relay.getStationId(), Handler2 -> {
                    if (Handler2.failed()) {
//                        eventbus.send(LoggerVerticle.ADDRESS, "Fail get Station");
                    } else {
                        Station station = Json.decodeValue(Handler2.result().body().toString(), Station.class);
                        if (relay.getStatus()) {
                            eventbus.send(InterrupterVerticle.ADDRESS, station.getIp() + "," + station.getPort() + "," + relay.getCode() + "," + InterrupterVerticle.TURN_OFF, HandlerInterrupOff -> {
                                String repuesta = HandlerInterrupOff.result().body().toString();
                                changeState(relay, repuesta);
                                sendResponse(r, repuesta, relay);
                            });
                        } else {
                            eventbus.send(InterrupterVerticle.ADDRESS, station.getIp() + "," + station.getPort() + "," + relay.getCode() + "," + InterrupterVerticle.TURN_ON, HandlerInterrupOn -> {
                                String respuesta = HandlerInterrupOn.result().body().toString();
                                changeState(relay, respuesta);
                                sendResponse(r, respuesta, relay);
                            });
                        }
                    }
                });

            }
        });
    }

    /**
     * envia la respuesta del on/off del relay
     * @param r
     * @param response solo puede ser OK o TIMEOUT
     * @param relay
     */
    public void sendResponse(RoutingContext r, String response, Relay relay) {
        JsonObject json = new JsonObject();
        json.put("res", response);
        json.put("code", relay.getCode());
        json.put("name", relay.getName());
        r.response()
                .putHeader("content-type", "application/json; charset=utf-8")
                .end(Json.encodePrettily(json));
    }

    /**
     * cambia el estatus de relay 
     * si reposnse ok cambia el estaus del relay
     * llama el metodo edit relay del DBClientVerticle 
     * @param relay
     * @param response
     */
    public void changeState(Relay relay, String response) {
        if (response.equals("OK")) {
            if (relay.getStatus()) {
                relay.setStatus(false);
            } else {
                relay.setStatus(true);
            }
            eventbus.send(DBClientVerticle.QUERY_EDIT_RELAY, Json.encodePrettily(relay), Handler -> {
                if (Handler.failed()) {
//                    eventbus.send(LoggerVerticle.ADDRESS, "Fail edit relay");
                } else {
//                    eventbus.send(LoggerVerticle.ADDRESS, "Ok edit relay");
                }
            });
        }
    }

    /**
     * edita un relay
     * @param r
     */
    public void editRelay(RoutingContext r) {
        Relay rele = Json.decodeValue(r.getBodyAsString(), Relay.class);
        double id = Double.parseDouble(r.request().getParam("id"));
        rele.setId(id);
        eventbus.send(DBClientVerticle.QUERY_EDIT_RELAY, Json.encodePrettily(rele), Handler -> {
            if (Handler.failed()) {
//                eventbus.send(LoggerVerticle.ADDRESS, "Fail edit relay");
            } else {
//                eventbus.send(LoggerVerticle.ADDRESS, "Ok edit relay");
            }
        });
        r.response().setStatusCode(204).end();
    }

    /**
     * agrega un relay
     * @param r
     */
    public void addRelay(RoutingContext r) {
        Relay rele = Json.decodeValue(r.getBodyAsString(), Relay.class);
        eventbus.send(DBClientVerticle.QUERY_ADD_RELAY, Json.encodePrettily(rele), Handler -> {
            String respuesta = Handler.result().body().toString();
            r.response()
                .putHeader("content-type", "text/plain")
                .end(respuesta);
        });
    }

    /**
     * elimina un relay
     * @param r
     */
    private void delRelay(RoutingContext r) {
        String id = r.request().getParam("id");
        eventbus.send(DBClientVerticle.QUERY_DEL_RELAY, Json.encodePrettily(id), Handler -> {
            if (Handler.failed()) {
//                eventbus.send(LoggerVerticle.ADDRESS, "Fail edit station");
            } else {
//                eventbus.send(LoggerVerticle.ADDRESS, "Ok edit station");
            }
        });
        r.response().setStatusCode(204).end();
    }

    /**
     * elimina una estacion
     * @param r
     */
    private void delStation(RoutingContext r) {
        String id = r.request().getParam("id");
        eventbus.send(DBClientVerticle.QUERY_DEL_STATION, Json.encodePrettily(id), Handler -> {
            if (Handler.failed()) {
//                eventbus.send(LoggerVerticle.ADDRESS, "Fail edit station");
            } else {
//                eventbus.send(LoggerVerticle.ADDRESS, "Ok edit station");
            }
        });
        r.response().setStatusCode(204).end();
    }

    /**
     * obtiene todo los relays de una estacion
     *
     * @param r
     */
    public void getRelays(RoutingContext r) {
        String idStation = r.request().getParam("idStation");
        eventbus.send(DBClientVerticle.QUERY_ADDRESS, DBClientVerticle.QUERY_STATION_RELAYS + "," + idStation, Handler -> {
            if (Handler.failed()) {
//                eventbus.send(LoggerVerticle.ADDRESS, "Fail get relay");
            } else {
                Relay[] relays = Json.decodeValue(Handler.result().body().toString(), Relay[].class);
                r.response()
                        .putHeader("content-type", "application/json; charset=utf-8")
                        .end(Json.encodePrettily(relays));
            }
        });
    }

    /**
     * agrega una estacion
     * @param r
     */
    public void addStation(RoutingContext r) {
        Station station = Json.decodeValue(r.getBodyAsString(), Station.class);
        eventbus.send(DBClientVerticle.QUERY_ADD_STATION, Json.encodePrettily(station), Handler -> {
            if (Handler.failed()) {
//                eventbus.send(LoggerVerticle.ADDRESS, "Fail add station");
            } else {
//                eventbus.send(LoggerVerticle.ADDRESS, "Ok add station");
            }
        });
        r.response()
                .setStatusCode(201)
                .putHeader("content-type", "application/json; charset=utf-8")
                .end(Json.encodePrettily(station));
//        r.response().setStatusCode(204).end();
    }

    /**
     * edita una estacion
     * @param r
     */
    public void updateStation(RoutingContext r) {
        Station station = Json.decodeValue(r.getBodyAsString(), Station.class);
        double id = Double.parseDouble(r.request().getParam("id"));
        station.setId(id);
        eventbus.send(DBClientVerticle.QUERY_EDIT_STATION, Json.encodePrettily(station), Handler -> {
            if (Handler.failed()) {
//                eventbus.send(LoggerVerticle.ADDRESS, "Fail add station");
            } else {
//                eventbus.send(LoggerVerticle.ADDRESS, "Ok add station");
            }
        });
        r.response().setStatusCode(204).end();
    }

    /**
     * obtiene todas las estaciones de la BD
     * @param r
     */
    public void getStation(RoutingContext r) {
        eventbus.send(DBClientVerticle.QUERY_ADDRESS, DBClientVerticle.QUERY_ALL_STATIONS, Handler -> {
            if (Handler.failed()) {
//                eventbus.send(LoggerVerticle.ADDRESS, "Fail get all stations");
            } else {
                Station[] updatedStations = Json.decodeValue((String) Handler.result().body(), Station[].class);
                r.response()
                        .putHeader("content-type", "application/json; charset=utf-8")
                        .end(Json.encodePrettily(updatedStations));
            }
        });
    }
}
