package co.gov.sgc.powercontrol.verticles;

import co.gov.sgc.powercontrol.util.ResponseCode;
import io.vertx.core.AbstractVerticle;
import java.util.Random;

/**
 *
 * @author Julian Peña
 */
public class InterrupterVerticle extends AbstractVerticle {

    public static final String ADDRESS = "InterrupterAddress";

    public static final int TURN_OFF = 0;
    public static final int TURN_ON = 1;

    @Override
    public void start() throws Exception {

        super.start();

        vertx.eventBus().consumer(ADDRESS, interrupterHandler -> {

            String[] message = ((String) interrupterHandler.body()).split(",");

            String ip = message[0];
            int port = Integer.parseInt(message[1]);

            String relayCode = message[2];
            String action = message[3];

            String rawCommand = relayCode + action + computeChecksum(relayCode + action);
            String command = rawCommand + "\r\n";
            
            vertx.eventBus().send(LoggerVerticle.ADDRESS, "Sending command " + rawCommand + " to station at " + ip + ":" + port);
            
            Random random = new Random(System.currentTimeMillis());

            vertx.eventBus().send(TCPClientVerticle.ADDRESS, ip + "," + port + "," + command, replyHandler -> {
                if (replyHandler.failed()) {
                    vertx.eventBus().send(LoggerVerticle.ADDRESS, "Command" + command + " was not sent for station at " + ip + ":" + port);
                } else {
                    try {
                        String response = ((String) replyHandler.result().body()).trim();
                        switch (ResponseCode.valueOf(response)) {
                            case OK:
                                vertx.eventBus().send(LoggerVerticle.ADDRESS, "Station at " + ip + ":" + port + " sent " + response);
                                vertx.eventBus().send(interrupterHandler.replyAddress(), ResponseCode.OK.toString());
                                break;
                            case TIMEOUT:
                                vertx.eventBus().send(LoggerVerticle.ADDRESS, "Station at " + ip + ":" + port + " timeout");
                                vertx.eventBus().send(interrupterHandler.replyAddress(), ResponseCode.TIMEOUT.toString());
                                break;
                        }
                    } catch (IllegalArgumentException e) {
                        vertx.eventBus().send(interrupterHandler.replyAddress(), ResponseCode.INVALID.toString());
                    }
                }
            });
        });
    }

    /**
     * Calculates and appends the checksum for the given command
     *
     * @param command
     * @return
     */
    private int computeChecksum(String command) {

        int checksum = 0;

        for (int i = 0; i < command.length(); i++) {
            checksum += command.charAt(i);
        }

        return checksum;
    }

}
