package co.gov.sgc.powercontrol.verticles;

import co.gov.sgc.powercontrol.pojos.Relay;
import co.gov.sgc.powercontrol.pojos.Station;
import com.google.gson.Gson;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.jdbc.JDBCClient;
import io.vertx.ext.sql.SQLConnection;
import java.util.ArrayList;

/**
 *
 * @author Julian Peña
 */
public class DBClientVerticle extends AbstractVerticle {

    public static final String QUERY_ADDRESS = "DBClientVerticleGetAddress";
    public static final String PUT_ADDRESS = "DBClientVerticleSetAddress";

    public static final String QUERY_ALL_STATIONS = "getStations";
    public static final String QUERY_STATION_RELAYS = "getRelays";

    public static final String QUERY_ADD_STATION = "addStation";
    public static final String QUERY_EDIT_STATION = "editStation";
    public static final String QUERY_DEL_STATION = "delStation";
    public static final String QUERY_GET_STATION = "StationGet";

    public static final String QUERY_ADD_RELAY = "addRelay";
    public static final String QUERY_EDIT_RELAY = "editRelay";
    public static final String QUERY_DEL_RELAY = "delRelay";
    public static final String QUERY_GET_RELAY = "RelayGet";
    public static final String QUERY_GET_CODE = "CodeGet";

    private JDBCClient jdbcClient;

    private Gson gson;

    @Override
    public void start() throws Exception {

        super.start();

        HikariConfig hikariConfig = new HikariConfig("database.properties");
        HikariDataSource datasource = new HikariDataSource(hikariConfig);

        jdbcClient = JDBCClient.create(vertx, datasource);

        gson = new Gson();

        vertx.eventBus().consumer(QUERY_ADDRESS, addressHandler -> {

            String[] message = ((String) addressHandler.body()).split(",");
            String query = message[0];

            String parameter = "";
            if (message.length > 1) {
                parameter = message[1];
            }

            switch (query) {
                case QUERY_ALL_STATIONS: {
                    getStations(addressHandler.replyAddress());
                }
                break;

                case QUERY_STATION_RELAYS: {
                    getRelays(Long.parseLong(parameter), addressHandler.replyAddress());
                }
                break;

                default:
                    vertx.eventBus().send(LoggerVerticle.ADDRESS, "Invalid query: " + query);
                    break;
            }

        });
        //consumer crud ADD station
        vertx.eventBus().consumer(QUERY_ADD_STATION, handler -> {
            Station station = Json.decodeValue(handler.body().toString(), Station.class);
            addStation(handler.replyAddress(), station);
        });
        //consumer crud EDIT station
        vertx.eventBus().consumer(QUERY_EDIT_STATION, handler -> {
            Station station = Json.decodeValue(handler.body().toString(), Station.class);
            editStation(handler.replyAddress(), station);
        });
        //consumer crud DEL station
        vertx.eventBus().consumer(QUERY_DEL_STATION, handler -> {
            int id = Json.decodeValue(handler.body().toString(), Integer.class);
            delStation(handler.replyAddress(), id);
        });
        //consumer crud ADD relay
        vertx.eventBus().consumer(QUERY_ADD_RELAY, handler -> {
            Relay relay = Json.decodeValue(handler.body().toString(), Relay.class);
            addRelay(handler.replyAddress(), relay);
        });
        //consumer crud EDIT relay
        vertx.eventBus().consumer(QUERY_EDIT_RELAY, handler -> {
            Relay relay = Json.decodeValue(handler.body().toString(), Relay.class);
            editRelay(handler.replyAddress(), relay);
        });
        //consumer crud DEL ralay
        vertx.eventBus().consumer(QUERY_DEL_RELAY, handler -> {
            int id = Json.decodeValue(handler.body().toString(), Integer.class);
            delRelay(handler.replyAddress(), id);
        });
        //consumer crud get ralay
        vertx.eventBus().consumer(QUERY_GET_RELAY, handler -> {
            int id = Json.decodeValue(handler.body().toString(), Integer.class);
            getRelay(handler.replyAddress(), id);
        });
        //consumer crud get station
        vertx.eventBus().consumer(QUERY_GET_STATION, handler -> {
            int id = Json.decodeValue(handler.body().toString(), Integer.class);
            getStation(handler.replyAddress(), id);
        });
        //consumer get code avalaible
        vertx.eventBus().consumer(QUERY_GET_CODE, handler -> {
            int id = Integer.parseInt(handler.body().toString());
            availableCode(handler.replyAddress(), id);
        });
    }

    /**
     * get one Station
     *
     * @param replyAddress
     * @param idStation
     */
    private void getStation(String replyAddress, int idStation) {
        jdbcClient.getConnection(connectionHandler -> {

            if (connectionHandler.failed()) {
                vertx.eventBus().send(LoggerVerticle.ADDRESS, "Database problem. Couldn't get a connection");
                vertx.eventBus().send(replyAddress, "Error, please check the log");
            } else {
                SQLConnection connection = connectionHandler.result();
                connection.query("select id,name,ip,port from stations where id = " + idStation + ";", queryHandler -> {

                    if (queryHandler.failed()) {
                        vertx.eventBus().send(LoggerVerticle.ADDRESS, "Database problem. Couldn't query for relays");
                        return;
                    }
                    Station station = null;
                    for (JsonArray line : queryHandler.result().getResults()) {
                        station = new Station(line.getDouble(0), line.getString(1), line.getString(2), line.getInteger(3));
                    }
                    vertx.eventBus().send(replyAddress, gson.toJson(station));
                });
                connection.close();
            }
        });
    }

    /**
     * get one Relay
     * @param replyAddress
     */
    private void getRelay(String replyAddress, int idRelay) {
        jdbcClient.getConnection(connectionHandler -> {

            if (connectionHandler.failed()) {
                vertx.eventBus().send(LoggerVerticle.ADDRESS, "Database problem. Couldn't get a connection");
                vertx.eventBus().send(replyAddress, "Error, please check the log");
            } else {
                SQLConnection connection = connectionHandler.result();
                connection.query("select id,name,code,status,station_id from relays where id = " + idRelay + ";", queryHandler -> {
                    if (queryHandler.failed()) {
                        vertx.eventBus().send(LoggerVerticle.ADDRESS, "Database problem. Couldn't query for relays");
                        return;
                    }
                    Relay relay = null;
                    for (JsonArray line : queryHandler.result().getResults()) {
                        relay = new Relay(line.getDouble(0), line.getString(1), line.getString(2), line.getBoolean(3), line.getDouble(4));
                    }
                    vertx.eventBus().send(replyAddress, gson.toJson(relay));
                });
                connection.close();
            }
        });

    }

    /**
     * Return all the stations on database.
     * @param replyAddress The vertx event bus address on which the response
     * should be sent
     */
    private void getStations(String replyAddress) {

        jdbcClient.getConnection(connectionHandler -> {

            if (connectionHandler.failed()) {
                vertx.eventBus().send(LoggerVerticle.ADDRESS, "Database problem. Couldn't get a connection");
                vertx.eventBus().send(replyAddress, "Error, please check the log");
            } else {

                SQLConnection connection = connectionHandler.result();
                connection.query("select id,name,ip,port from stations", queryHandler -> {

                    if (queryHandler.failed()) {
                        vertx.eventBus().send(LoggerVerticle.ADDRESS, "Database problem. Couldn't query for stations");
                    } else {

                        ArrayList<Station> stations = new ArrayList<>();

                        for (JsonArray line : queryHandler.result().getResults()) {
                            Station station = new Station(line.getDouble(0), line.getString(1), line.getString(2), line.getInteger(3));
                            stations.add(station);
                        }

                        vertx.eventBus().send(replyAddress, gson.toJson(stations));
                    }

                });
                connection.close();
            }
        });
    }

    /**
     * Add station on the BD
     * @param replyAddress
     * @param station
     */
    private void addStation(String replyAddress, Station station) {
        jdbcClient.getConnection(connectionHandler -> {

            if (connectionHandler.failed()) {
                vertx.eventBus().send(LoggerVerticle.ADDRESS, "Database problem. Couldn't get a connection");
                vertx.eventBus().send(replyAddress, "Error, please check the log");
            } else {
                SQLConnection connection = connectionHandler.result();
                connection.execute("INSERT INTO stations ( name, ip, port ) "
                        + "VALUES ('" + station.getName() + "','" + station.getIp() + "'," + station.getPort() + ");", queryHandler -> {
                            if (queryHandler.failed()) {
                                vertx.eventBus().send(LoggerVerticle.ADDRESS, "Database problem. Couldn't query for stations add");
                            } else {
                                vertx.eventBus().send(LoggerVerticle.ADDRESS, "Ok query for stations add");
                            }

                        });
                connection.close();
            }
        });

    }

    /**
     * Edit station
     * @param replyAddress
     * @param station
     */
    private void editStation(String replyAddress, Station station) {
        jdbcClient.getConnection(connectionHandler -> {

            if (connectionHandler.failed()) {
                vertx.eventBus().send(LoggerVerticle.ADDRESS, "Database problem. Couldn't get a connection");
                vertx.eventBus().send(replyAddress, "Error, please check the log");
            } else {

                SQLConnection connection = connectionHandler.result();
                connection.execute("UPDATE stations  SET name = '" + station.getName() + "', ip='" + station.getIp() + "', port=" + station.getPort() + " where id=" + station.getId() + "", queryHandler -> {
                    if (queryHandler.failed()) {
                        vertx.eventBus().send(LoggerVerticle.ADDRESS, "Database problem. Couldn't query for stations edit");
                    } else {
                        vertx.eventBus().send(LoggerVerticle.ADDRESS, "Ok query for stations edit");
                    }
                });
                connection.close();
            }
        });
    }

    /**
     * elimina una station
     * @param replyAddress
     * @param id
     */
    private void delStation(String replyAddress, int id) {
        jdbcClient.getConnection(connectionHandler -> {

            if (connectionHandler.failed()) {
                vertx.eventBus().send(LoggerVerticle.ADDRESS, "Database problem. Couldn't get a connection");
                vertx.eventBus().send(replyAddress, "Error, please check the log");
            } else {
                SQLConnection connection = connectionHandler.result();
                connection.execute("DELETE FROM stations WHERE id = " + id + ";", queryHandler -> {
                    if (queryHandler.failed()) {
                        vertx.eventBus().send(LoggerVerticle.ADDRESS, "Database problem. Couldn't query for stations del");
                    } else {
                        vertx.eventBus().send(LoggerVerticle.ADDRESS, "Ok query for stations del");
                    }
                });
                connection.close();
            }
        });
    }

    /**
     * Return all the relays for the given station.
     * @param replyAddress The vertx event bus address on which the response
     * should be sent
     */
    private void getRelays(double stationID, String replyAddress) {
        jdbcClient.getConnection(connectionHandler -> {

            if (connectionHandler.failed()) {
                vertx.eventBus().send(LoggerVerticle.ADDRESS, "Database problem. Couldn't get a connection");
                vertx.eventBus().send(replyAddress, "Error, please check the log");
            } else {

                SQLConnection connection = connectionHandler.result();
                connection.query("select id,name,code,status from relays where station_id = " + stationID + ";", queryHandler -> {

                    if (queryHandler.failed()) {
                        vertx.eventBus().send(LoggerVerticle.ADDRESS, "Database problem. Couldn't query for relays");
                        return;
                    } else {
                        ArrayList<Relay> relays = new ArrayList<>();

                        for (JsonArray line : queryHandler.result().getResults()) {
                            Relay relay = new Relay(line.getDouble(0), line.getString(1), line.getString(2), line.getBoolean(3), stationID);
                            relays.add(relay);
                        }
                        vertx.eventBus().send(replyAddress, gson.toJson(relays));
                    }
                });
                connection.close();
            }
        });
    }

    /**
     * add new relay
     * @param replyAddress
     * @param relay
     */
    private void addRelay(String replyAddress, Relay relay) {
        jdbcClient.getConnection(connectionHandler -> {

            if (connectionHandler.failed()) {
                vertx.eventBus().send(LoggerVerticle.ADDRESS, "Database problem. Couldn't get a connection");
                vertx.eventBus().send(replyAddress, "Error, please check the log");
            } else {
                SQLConnection connection = connectionHandler.result();
                connection.query("SELECT count(code) FROM relays WHERE station_id =" + relay.getStationId() + "AND code = '" + relay.getCode() + "'", queryHandler -> {
                    int cantidad = 0;
                    for (JsonArray line : queryHandler.result().getResults()) {
                        cantidad = line.getInteger(0);
                    }
                    if (cantidad == 0) {
                        add(replyAddress, relay);
                    }
                    vertx.eventBus().send(replyAddress, cantidad);
                });
                connection.close();
            }
        });

    }

    private void add(String replyAddress, Relay relay) {
        jdbcClient.getConnection(connectionHandler -> {

            if (connectionHandler.failed()) {
                vertx.eventBus().send(LoggerVerticle.ADDRESS, "Database problem. Couldn't get a connection");
                vertx.eventBus().send(replyAddress, "Error, please check the log");
            } else {
                SQLConnection connection = connectionHandler.result();
                int aux = Cambio(relay.getStatus());
                connection.update("INSERT INTO relays ( code, name, status, station_id ) "
                        + "VALUES ('" + relay.getCode() + "','" + relay.getName() + "'," + aux + "," + relay.getStationId() + ")", queryHandlerAdd -> {
                        });
                connection.close();
            }
        });
    }

    /**
     * cambia el status del relay de true/false a 0/1
     *
     * @param status
     * @return
     */
    private int Cambio(boolean status) {
        int resp = 0;
        if (status == true) {
            resp = 1;
        }
        return resp;
    }

    /**
     * edit relay
     * @param replyAddress
     * @param relay
     */
    private void editRelay(String replyAddress, Relay relay) {
        jdbcClient.getConnection(connectionHandler -> {
            if (connectionHandler.failed()) {
                vertx.eventBus().send(LoggerVerticle.ADDRESS, "Database problem. Couldn't get a connection");
                vertx.eventBus().send(replyAddress, "Error, please check the log");
            } else {
                SQLConnection connection = connectionHandler.result();
                int aux = Cambio(relay.getStatus());
                connection.execute("UPDATE relays  SET name = '" + relay.getName() + "', status= " + aux + ", code = '" + relay.getCode() + "'  where id=" + relay.getId() + "", queryHandler -> {
                    if (queryHandler.failed()) {
                        vertx.eventBus().send(LoggerVerticle.ADDRESS, "Database problem. Couldn't query for stations edit");
                    } else {
                        vertx.eventBus().send(LoggerVerticle.ADDRESS, "Ok query for stations edit");
                    }

                });
                connection.close();
            }
        });

    }

    /**
     * delete relay
     * @param replyAddress
     * @param idRelay
     */
    private void delRelay(String replyAddress, int idRelay) {
        jdbcClient.getConnection(connectionHandler -> {
            if (connectionHandler.failed()) {
                vertx.eventBus().send(LoggerVerticle.ADDRESS, "Database problem. Couldn't get a connection");
                vertx.eventBus().send(replyAddress, "Error, please check the log");
            } else {
                SQLConnection connection = connectionHandler.result();
                connection.execute("delete from relays where id = " + idRelay + ";", queryHandler -> {
                    if (queryHandler.failed()) {
                        vertx.eventBus().send(LoggerVerticle.ADDRESS, "Database problem. Couldn't query for relay del");
                    } else {
                        vertx.eventBus().send(LoggerVerticle.ADDRESS, "Ok query for relay del");
                    }

                });
                connection.close();
            }
        });

    }

    /**
     * obtiene todos los code disponibles para una estacion
     * @param replyAddress
     * @param idStation
     */
    private void availableCode(String replyAddress, int idStation) {
        jdbcClient.getConnection(connectionHandler -> {
            if (connectionHandler.failed()) {
                vertx.eventBus().send(LoggerVerticle.ADDRESS, "Database problem. Couldn't get a connection");
                vertx.eventBus().send(replyAddress, "Error, please check the log");
            } else {
                SQLConnection connection = connectionHandler.result();
                connection.query("SELECT code FROM relays WHERE station_id = " + idStation + ";", queryHandler -> {
                    if (queryHandler.failed()) {
                        vertx.eventBus().send(LoggerVerticle.ADDRESS, "Database problem. Couldn't query avalaible code");
                    } else {
                        ArrayList<String> codeAvalaible = new ArrayList<>();

                        for (JsonArray line : queryHandler.result().getResults()) {
                            codeAvalaible.add(line.getString(0));
                        }
                        vertx.eventBus().send(replyAddress, gson.toJson(codeAvalaible));
                    }

                });
                connection.close();
            }
        });
    }

}
