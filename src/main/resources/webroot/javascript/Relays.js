/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var station;

$(function () {
    iniciarDialogRelay();

});
/*
 * agrega un rele
 * envia el objeto en json
 * @param {type} name
 * @param {type} code
 * @param {type} status
 * @returns {undefined}
 */
function crearRelay(name, code, status) {
    $.ajax({
        method: "POST",
        url: "/api/Relays/",
        data: JSON.stringify({name: name, code: code, status: status, stationId: station})
    }).done(function (data) {
        if (data === '0') {
            loadRelays(station);
        } else {
            alert("El code " + code + " ya esta en uso");
        }
    });
//    $.post("/api/Relays/", JSON.stringify({name: name, code: code, status: status, stationId: station}), function (data) {
//        alert(data.resp);
//    }, "json");
}
/*
 * edita un rele
 * @param {type} name
 * @param {type} code
 * @param {type} status
 * @returns {undefined}
 */
function editarRelay(id, name, code, status) {
    $.ajax({
        method: "PUT",
        url: "/api/Relays/" + id,
        data: JSON.stringify({name: name, code: code, status: status})
    }).done(function () {
        loadRelays(station);
    });
}
/*
 * programa el boton para listar los relays 
 * llama el metodo loadRelays
 * cambia el valor de la variable station
 * @returns {undefined}
 */
function listarRelays() {
    $(".listar-relays").unbind().click(function () {
        botonesRelays();
        var idStation = $(this).data("id");
        station = idStation;
        loadRelays(idStation);
    });
}
/*
 * asigna al boton eliminar la funcion de eliminar relay
 * @returns {undefined}
 */
function eliminarRele() {
    $(".eliminar-relay").unbind().click(function () {
        var id = $(this).data("id");
        eliminarRelay(id);
    });
}
/*
 * elimina un relay
 * @param {type} id
 * @returns {undefined}
 */
function eliminarRelay(id) {
    $.ajax({
        method: "DELETE",
        url: "/api/Relays/" + id
    }).done(function () {
        loadRelays(station);
    });
}
/*
 * obtene una lista de la BD con los code usados en esa estacion
 * elimina del select los code que obtuvo en la lista y solo quedan los que esta sin usar
 * @param {type} stationId
 * @returns {undefined}
 */
function loadAvailableCode(stationId) {
    $.getJSON("/api/Relays/" + stationId, function (data) {
        console.log(data);
        var i;
        for (i = 0; i < data.length; i++) {
            $("#code-relay option[value='" + data[i] + "']").remove();
//            $("#code-relay").find(data[i]).remove();
        }
    });
}
/**
 * llena el select desde S0! hasta S15
 * @returns {undefined}
 */
function loadSelectCode() {
    $("#code-relay").children().remove();
//    $("#code-relay").empty();
    $("#code-relay").append('<option value="S01">S01</option>');
    $("#code-relay").append('<option value="S02">S02</option>');
    $("#code-relay").append('<option value="S03">S03</option>');
    $("#code-relay").append('<option value="S04">S04</option>');
    $("#code-relay").append('<option value="S05">S05</option>');
    $("#code-relay").append('<option value="S06">S06</option>');
    $("#code-relay").append('<option value="S07">S07</option>');
    $("#code-relay").append('<option value="S08">S08</option>');
    $("#code-relay").append('<option value="S09">S09</option>');
    $("#code-relay").append('<option value="S10">S10</option>');
    $("#code-relay").append('<option value="S11">S11</option>');
    $("#code-relay").append('<option value="S12">S12</option>');
    $("#code-relay").append('<option value="S13">S13</option>');
    $("#code-relay").append('<option value="S14">S14</option>');
    $("#code-relay").append('<option value="S15">S15</option>');
}
/*
 * hace la peticion get
 * los objetos los recibe en un json 
 * carga en la tabla los relays
 * @param {type} idStation
 * @returns {undefined}
 */
function loadRelays(idStation) {
    loadSelectCode();
    loadAvailableCode(idStation);
    $("#thead").children().remove();
    $("<tr><td> Name </td><td> Code </td><td> Status </td><td> On-Off </td><td> Actions </td></tr>").appendTo("#thead");
    var i;
    var estado;
    $("#body").children().remove();
    $.getJSON("/api/Estaciones/" + idStation, function (data) {
        for (i = 0; i < data.length; i++) {
            estado = Estado(data[i].status);
            $("<tr><td>" + data[i].name + "</td><td>" + data[i].code + "<td>" + estado + "</td>" + "<td>" +
                    "<button class='btn btn-danger btn-sm onOff-relay' data-id='" + data[i].id + "'>" +
                    "   <span class='glyphicon glyphicon-off'></span>" +
                    "</button>" +
                    "</td><td>" +
                    "<button data-action='edit' class='btn btn-primary btn-sm editar-relay' " +
                    "data-toggle='modal' " +
                    "data-target='#relaymodal' " +
                    "data-id='" + data[i].id + "' " +
                    "data-nombre='" + data[i].name + "' " +
                    "data-code='" + data[i].code + "' " +
                    "data-status='" + data[i].status + "'>" +
                    "<span class='glyphicon glyphicon-pencil'></span>" +
                    "</button>" +
                    "&nbsp;" +
                    "<button class='btn btn-danger btn-sm eliminar-relay' data-id='" + data[i].id + "'>" +
                    "   <span class='glyphicon glyphicon-minus'></span>" +
                    "</button></td>" +
                    "</tr>").appendTo("#body");
        }
        eliminarRele();
        OnOff();
    });
}
/*
 * cambia el bool del relay a on/off
 * @param {type} status
 * @returns {String} on?off
 */
function Estado(status) {
    if (status === true) {
        return "On";
    } else {
        return "Off";
    }
}
/*
 * metodo para encender/apagar el relay
 * @returns {undefined}
 */
function OnOff() {
    $(".onOff-relay").unbind().click(function () {
        var idRele = $(this).data("id");
        onOffRele(idRele);
    });
}
/**
 * por post manda el id del relay que se quiere encender o apagar
 * reponde con timeout o ok
 * @param {type} idRelay
 * @returns {undefined}
 */
function onOffRele(idRelay) {
    $.ajax({
        method: "POST",
        url: "/api/Relays/" + idRelay
    }).done(function (data) {
        console.log(data);
        if (data === "OK") {
            loadRelays(station);
        } else {
            alert(data.res + " Relay "+ data.name + " Code = "+data.code);
        }
    });
}

/*
 * botones para los relays
 * pone el titulo en el div
 */
function botonesRelays() {
    $("#botones").children().remove();
    $("<h1> Relays </h1>").appendTo("#botones");
    $("<button class='pull-left btn btn-primary volver' data-action='volver'>" +
            "<span class='glyphicon glyphicon-circle-arrow-left'></span>").appendTo("#botones");
    $("<button class='pull-right btn btn-primary add-relay' data-action='add' data-toggle='modal', data-target='#relaymodal'>" +
            "<span class='glyphicon glyphicon-plus'></span>").appendTo("#botones");
    Regresar();
}
/*
 * 
 * volver a cargar las estaciones
 */
function Regresar() {
    $(".volver").unbind().click(function () {
        load();
    });
}
/**
 * incia el modal para agregar y editar relays
 * @returns {undefined}
 */
function iniciarDialogRelay() {
    $("#relaymodal").on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var action = button.data('action');
        var id = button.data('id');
        var btnGuardar = $("#btn-guardarRelay");
        btnGuardar.unbind();
        var modal = $(this);
        if (action === "add") {
            modal.find('.modal-title').text("Agregar Relay");
            modal.find('#nombre-relay').val("");
            btnGuardar.click(function () {
                if (validarNombre($("#nombre-relay").val())) {
                    if (validarCode($("#code-relay").val())) {
                        crearRelay($("#nombre-relay").val(), $("#code-relay").val(), $("#status-relay").val());
                        $('#relaymodal').modal('toggle');
                    }else{
                        alert("Seleccione un code");
                    }
                } else {
                    alert("Ingrese un nombre");
                }
            });
        } else {
            modal.find('.modal-title').text("Editar Relay");
            modal.find('#nombre-relay').val(button.data("nombre"));
            modal.find('#status-relay').selected(text(button.data("status")));
            btnGuardar.click(function () {
                if (validarNombre(validarNombre($("#nombre-relay").val()))) {
                    editarRelay(id, $("#nombre-relay").val(), $("#code-relay").val(), $("#status-relay").val());
                    $('#relaymodal').modal('toggle');
                } else {
                    alert("Ingrese un nombre");
                }
            });
        }
    });
}
/**
 * valida si se selecciono un code
 * @param {type} code
 * @returns {Boolean}
 */
function validarCode(code) {
    if (code !== null) {
        return true;
    } else {
        return false;
    }
}






