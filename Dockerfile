FROM java:alpine
MAINTAINER abustos
COPY PowerControl .
WORKDIR /target
#RUN javac Hello.java
CMD ["java","-jar","PowerControl-1.0-jar-with-dependencies.jar"]
EXPOSE 8090
